#Join 2 arrays without using inbuilt functions.
#Join 2 arrays using inbuilt functions. 
#15 Jan 2020 Qno4 and Qno 5
class Join2array

	@@array1=[1,2,3,4,5];
	@@array2=[6,7,8,9];
	
	def joinByPlus
	
		puts "First array = #{@@array1} \nSecond array = #{@@array2} \njoining by + operator #{@@array1+@@array2} \n";
	end

	def joinByMethod
		@@array1.concat(@@array2)
		puts " First array = #{@@array1} \n Second array = #{@@array2} \n joining by concat methods #{@@array1} \n";

	end
	
end	
object=Join2array.new;
puts "............................."
object.joinByPlus

puts "............................."
object.joinByMethod

