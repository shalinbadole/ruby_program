#Use the map function to double all the elements in the array.
# 15 jan 2020 Qno6 
class DoubleTheElement
  @@arr = [1,2,3,4,"c"]
  def byCollect
    puts "original elements of array =#{@@arr} \nUsing collect method \nelements are getting doubled = #{@@arr.collect{|x| x*2}}"   
  end
  def byMapIndex
    puts "original elements of array =#{@@arr} \nUsing map method \nElements are getting doubled = #{@@arr.map{|x | x * 2}}"   
  end
  def doubleNum
    i=0;
    while i<@@arr.length
      if Fixnum === @@arr[i]
        puts "#{@@arr[i]} is a integer"
      end
    i=i+1;
  end     
  end
  end
  object=DoubleTheElement.new;
  puts "............................."
  object.byCollect
  puts "............................."
  object.byMapIndex
  object.doubleNum